# Prefix Tree (Trie) Implementation in C

## Quick Start

```console
$ ./build.sh
$ xdg-open trie.svg
$ ./trie complete Ap
```

## References
- https://en.wikipedia.org/wiki/Trie
